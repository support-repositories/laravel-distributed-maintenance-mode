# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.2.0] - 2024-04-11
### Added
- Laravel 11 support

### Removed
- Laravel 9 support

## [1.1.0] - 2023-10-02
### Added
- Laravel 10 support

## [1.0.0] - 2023-01-23
### Added
- Two news drivers(Redis and S3) that can be used to set a Laravel application into maintenance mode
