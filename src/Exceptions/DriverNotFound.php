<?php

declare(strict_types=1);

namespace Despark\DistributedMaintenanceMode\Exceptions;

use Exception;

class DriverNotFound extends Exception
{
}
